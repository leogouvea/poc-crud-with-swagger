## Description

[Nest](https://github.com/nestjs/nest) Poc using TS, swagger, express.

## Installation

```bash
$ npm install
```

## Running the app

```bash
# development
$ npm run start

# watch mode
$ npm run start:dev

# production mode
$ npm run start:prod
```

## Test

```bash
# unit tests
$ npm run test

# e2e tests
$ npm run test:e2e

# test coverage
$ npm run test:cov
```

## ENVS

```bash
# Add your application configuration to your .env file in the root of your project: (example in .env.exmaple)
MONGO_URL=YOUR_MONGO_URL
ENVIRONMENT=CURRENT_ENVIRONMENT
```

FROM node:12.19.0-alpine3.9

WORKDIR /usr/src/app

COPY package*.json ./
COPY .env ./

RUN npm install glob rimraf

RUN npm install --only=development

COPY . .

RUN npm test 
RUN npm run build
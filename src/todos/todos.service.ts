import { Injectable } from '@nestjs/common'
import { CreateTodoDto } from './dto/create-todo.dto'
import { UpdateTodoDto } from './dto/update-todo.dto'
import { Todo } from './entities/todo.entity'
import { TodosRepository } from './todos.repository'

@Injectable()
export class TodosService {
  constructor(private readonly todosRepository: TodosRepository) {}

  async create(createTodoDto: CreateTodoDto): Promise<Todo> {
    return this.todosRepository.create(createTodoDto)
  }

  async findAll(): Promise<Todo[]> {
    return this.todosRepository.find({})
  }

  async findOne(id: string) {
    return this.todosRepository.findOne({ _id: id })
  }

  async update(id: string, updateTodoDto: UpdateTodoDto) {
    return this.todosRepository.findOneAndUpdate({ _id: id }, updateTodoDto)
  }

  async remove(id: string) {
    return this.todosRepository.deleteMany({ _id: id })
  }
}

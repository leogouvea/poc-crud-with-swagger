import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
} from '@nestjs/common'
import { TodosService } from './todos.service'
import { CreateTodoDto } from './dto/create-todo.dto'
import { UpdateTodoDto } from './dto/update-todo.dto'
import { ApiOperation, ApiResponse, ApiTags } from '@nestjs/swagger'
import { Todo } from './entities/todo.entity'

@ApiTags('todos')
@Controller('todos')
export class TodosController {
  constructor(private readonly todosService: TodosService) {}

  @Post()
  @ApiOperation({ summary: 'Create cat' })
  create(@Body() createTodoDto: CreateTodoDto) {
    return this.todosService.create(createTodoDto)
  }

  @Get()
  @ApiOperation({ summary: 'Find all todos' })
  @ApiResponse({
    status: 200,
    type: [Todo],
  })
  findAll() {
    return this.todosService.findAll()
  }

  @Get(':id')
  @ApiOperation({ summary: 'Find todo by id' })
  @ApiResponse({
    status: 200,
    type: Todo,
  })
  findOne(@Param('id') id: string) {
    return this.todosService.findOne(id)
  }

  @Patch(':id')
  @ApiOperation({ summary: 'Update todo by id' })
  @ApiResponse({
    status: 200,
    type: Todo,
  })
  async update(@Param('id') id: string, @Body() updateTodoDto: UpdateTodoDto) {
    return this.todosService.update(id, updateTodoDto)
  }

  @Delete(':id')
  @ApiOperation({ summary: 'Remove todo by id' })
  @ApiResponse({
    status: 200,
    type: 'true',
  })
  remove(@Param('id') id: string) {
    return this.todosService.remove(id)
  }
}

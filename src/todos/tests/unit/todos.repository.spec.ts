import { getModelToken } from '@nestjs/mongoose'
import { Test } from '@nestjs/testing'
import { FilterQuery } from 'mongoose'
import { Todo } from '../../entities/todo.entity'
import { TodosRepository } from '../../todos.repository'
import { todoStub } from '../stubs/todo.stub'
import { TodoModel } from '../support/todo.model'

describe('TodosRepository', () => {
  let todosRepository: TodosRepository

  describe('find operations', () => {
    let todoModel: TodoModel
    let todoFilterQuery: FilterQuery<Todo>

    beforeEach(async () => {
      const moduleRef = await Test.createTestingModule({
        providers: [
          TodosRepository,
          {
            provide: getModelToken(Todo.name),
            useClass: TodoModel,
          },
        ],
      }).compile()

      todosRepository = moduleRef.get<TodosRepository>(TodosRepository)
      todoModel = moduleRef.get<TodoModel>(getModelToken(Todo.name))

      todoFilterQuery = {
        _id: todoStub()._id,
      }

      jest.clearAllMocks()
    })

    describe('findOne', () => {
      describe('when findOne is called', () => {
        let todo: Todo

        beforeEach(async () => {
          jest.spyOn(todoModel, 'findOne')
          todo = await todosRepository.findOne(todoFilterQuery)
        })

        test('then it should call the todoModel', () => {
          expect(todoModel.findOne).toHaveBeenCalledWith(todoFilterQuery, {
            __v: 0,
          })
        })

        test('then it should return a todo', () => {
          expect(todo).toEqual(todoStub())
        })
      })
    })

    describe('find', () => {
      describe('when find is called', () => {
        let todos: Todo[]

        beforeEach(async () => {
          jest.spyOn(todoModel, 'find')
          todos = await todosRepository.find(todoFilterQuery)
        })

        test('then it should call the todoModel', () => {
          expect(todoModel.find).toHaveBeenCalledWith(todoFilterQuery, {
            __v: 0,
          })
        })

        test('then it should return a todo', () => {
          expect(todos).toEqual([todoStub()])
        })
      })
    })

    describe('findOneAndUpdate', () => {
      describe('when findOneAndUpdate is called', () => {
        let todo: Todo

        beforeEach(async () => {
          jest.spyOn(todoModel, 'findOneAndUpdate')
          todo = await todosRepository.findOneAndUpdate(
            todoFilterQuery,
            todoStub(),
          )
        })

        test('then it should call the todoModel', () => {
          expect(todoModel.findOneAndUpdate).toHaveBeenCalledWith(
            todoFilterQuery,
            todoStub(),
            { new: true },
          )
        })

        test('then it should return a todo', () => {
          expect(todo).toEqual(todoStub())
        })
      })
    })
  })

  describe('create operations', () => {
    beforeEach(async () => {
      const moduleRef = await Test.createTestingModule({
        providers: [
          TodosRepository,
          {
            provide: getModelToken(Todo.name),
            useValue: TodoModel,
          },
        ],
      }).compile()

      todosRepository = moduleRef.get<TodosRepository>(TodosRepository)
    })

    describe('create', () => {
      describe('when create is called', () => {
        let todo: Todo
        let saveSpy: jest.SpyInstance
        let constructorSpy: jest.SpyInstance

        beforeEach(async () => {
          saveSpy = jest.spyOn(TodoModel.prototype, 'save')
          constructorSpy = jest.spyOn(TodoModel.prototype, 'constructorSpy')
          todo = await todosRepository.create(todoStub())
        })

        test('then it should call the todoModel', () => {
          expect(saveSpy).toHaveBeenCalled()
          expect(constructorSpy).toHaveBeenCalledWith(todoStub())
        })

        test('then it should return a todo', () => {
          expect(todo).toEqual(todoStub())
        })
      })
    })
  })
})

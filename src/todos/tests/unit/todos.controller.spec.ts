import { Test } from '@nestjs/testing'
import { CreateTodoDto } from '../../dto/create-todo.dto'
import { UpdateTodoDto } from '../../dto/update-todo.dto'
import { Todo } from '../../entities/todo.entity'
import { TodosController } from '../../todos.controller'
import { TodosService } from '../../todos.service'
import { todoStub } from '../stubs/todo.stub'

jest.mock('../../todos.service')

describe('TodosController', () => {
  let todosController: TodosController
  let todosService: TodosService

  beforeEach(async () => {
    const moduleRef = await Test.createTestingModule({
      imports: [],
      controllers: [TodosController],
      providers: [TodosService],
    }).compile()

    todosController = moduleRef.get<TodosController>(TodosController)
    todosService = moduleRef.get<TodosService>(TodosService)
    jest.clearAllMocks()
  })

  describe('findOne', () => {
    describe('when findOne is called', () => {
      let todo: Todo

      beforeEach(async () => {
        todo = await todosController.findOne(todoStub()._id)
      })

      test('then it should call todosService', () => {
        expect(todosService.findOne).toBeCalledWith(todoStub()._id)
      })

      test('then is should return a todo', () => {
        expect(todo).toEqual(todoStub())
      })
    })
  })

  describe('findAll', () => {
    describe('when findAll is called', () => {
      let todos: Todo[]

      beforeEach(async () => {
        todos = await todosController.findAll()
      })

      test('then it should call todosService', () => {
        expect(todosService.findAll).toHaveBeenCalled()
      })

      test('then it should return todos', () => {
        expect(todos).toEqual([todoStub()])
      })
    })
  })

  describe('create', () => {
    describe('when create is called', () => {
      let todo: Todo
      let createTodoDto: CreateTodoDto

      beforeEach(async () => {
        createTodoDto = {
          text: todoStub().text,
        }
        todo = await todosController.create(createTodoDto)
      })

      test('then it should call todosService', () => {
        expect(todosService.create).toHaveBeenCalledWith(createTodoDto)
      })

      test('then it should return a todo', () => {
        expect(todo).toEqual(todoStub())
      })
    })
  })

  describe('update', () => {
    describe('when update is called', () => {
      let todo: Todo
      let updateTodoDto: UpdateTodoDto

      beforeEach(async () => {
        updateTodoDto = {
          text: 'Bye World!',
        }
        todo = await todosController.update(todoStub()._id, updateTodoDto)
      })

      test('then it should call todosService', () => {
        expect(todosService.update).toHaveBeenCalledWith(
          todoStub()._id,
          updateTodoDto,
        )
      })

      test('then it should return a todo', () => {
        expect(todo).toEqual(todoStub())
      })
    })
  })
})

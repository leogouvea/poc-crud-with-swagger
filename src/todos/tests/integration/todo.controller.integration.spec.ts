import * as request from 'supertest'
import { Test } from '@nestjs/testing'
import { Connection } from 'mongoose'
import { DatabaseService } from '../../../database/database.service'
import { todoIntegrationStub, todoStub } from '../stubs/todo.stub'
import { AppModule } from '../../../app.module'

describe('TodoContreller integration', () => {
  let dbConnection: Connection
  let httpServer: any
  let app: any
  const collectionName = 'todos'

  beforeAll(async () => {
    const moduleFixture = await Test.createTestingModule({
      imports: [AppModule],
    }).compile()

    app = moduleFixture.createNestApplication()
    await app.init()
    dbConnection = moduleFixture
      .get<DatabaseService>(DatabaseService)
      .getDbHandle()
    httpServer = app.getHttpServer()
  })

  afterAll(async () => {
    await app.close()
  })

  afterEach(async () => {
    await dbConnection.collection(collectionName).deleteMany({})
  })

  describe('find all todos', () => {
    it('should return all todos', async () => {
      await dbConnection
        .collection(collectionName)
        .insertOne(todoIntegrationStub())
      const response = await request(httpServer).get(`/${collectionName}`)

      expect(response.status).toBe(200)
      expect(response.body).toMatchObject([todoIntegrationStub()])
    })
  })

  describe('create new todo', () => {
    it('should create a todo', async () => {
      const createTodoRequest = todoStub()
      delete createTodoRequest._id
      const response = await request(httpServer)
        .post(`/${collectionName}`)
        .send(createTodoRequest)

      expect(response.status).toBe(201)
      expect(response.body).toMatchObject(createTodoRequest)
    })
  })

  describe('find new todo', () => {
    it('should find one todo', async () => {
      const createTodoRequest = todoStub()
      delete createTodoRequest._id
      const response = await dbConnection
        .collection(collectionName)
        .insertOne({ text: createTodoRequest.text })

      const todo = await request(httpServer).get(
        `/${collectionName}/${response.insertedId}`,
      )
      expect(todo.body).toMatchObject(createTodoRequest)
    })
  })

  describe('update todo', () => {
    it('should update a todo', async () => {
      const createTodoRequest = todoStub()
      const response = await dbConnection
        .collection(collectionName)
        .insertOne({ text: createTodoRequest.text })

      const todoIntegration = todoIntegrationStub()
      const todo = await request(httpServer)
        .patch(`/${collectionName}/${response.insertedId}`)
        .send(todoIntegration)
      expect(todo.status).toBe(200)
      expect(todo.body).toMatchObject(todoIntegration)
    })
  })

  describe('delete todo', () => {
    it('should delete a todo', async () => {
      const createTodoRequest = todoStub()
      const response = await dbConnection
        .collection(collectionName)
        .insertOne({ text: createTodoRequest.text })

      const todo = await request(httpServer).delete(
        `/${collectionName}/${response.insertedId}`,
      )
      expect(todo.status).toBe(200)
    })
  })
})

import { Todo } from '../../entities/todo.entity'

export const todoStub = (): Todo => {
  return {
    _id: '123',
    text: 'Hello world!',
  }
}

export const todoIntegrationStub = () => {
  return {
    text: 'bye world!',
  }
}

import { MockModel } from '../../../database/test/mock.model'
import { Todo } from '../../entities/todo.entity'
import { todoStub } from '../stubs/todo.stub'

export class TodoModel extends MockModel<Todo> {
  protected entityStub = todoStub()
}

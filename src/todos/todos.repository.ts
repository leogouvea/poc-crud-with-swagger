import { Model } from 'mongoose'
import { Injectable } from '@nestjs/common'
import { InjectModel } from '@nestjs/mongoose'

import { EntityRepository } from '../database/entity.repository'
import { Todo, TodoDocument } from './entities/todo.entity'

@Injectable()
export class TodosRepository extends EntityRepository<TodoDocument> {
  constructor(@InjectModel(Todo.name) todoModel: Model<TodoDocument>) {
    super(todoModel)
  }
}

import { ApiProperty } from '@nestjs/swagger'
import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose'
import { Document } from 'mongoose'

export type TodoDocument = Todo & Document

@Schema()
export class Todo {
  @ApiProperty({
    example: '6311a070d1c614269de9d29d',
    description: 'Todo Id',
  })
  _id: string

  @ApiProperty({
    example: 'Passear com o cão',
    description: 'Text of the task',
  })
  @Prop()
  text: string
}

export const TodoSchema = SchemaFactory.createForClass(Todo)

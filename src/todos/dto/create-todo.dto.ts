import { ApiProperty } from '@nestjs/swagger'

export class CreateTodoDto {
  @ApiProperty({
    example: 'Passear com o cão',
    description: 'Text of the task',
  })
  readonly text: string
}

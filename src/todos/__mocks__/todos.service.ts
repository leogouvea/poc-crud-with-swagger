import { todoStub } from '../tests/stubs/todo.stub'

export const TodosService = jest.fn().mockReturnValue({
  create: jest.fn().mockResolvedValue(todoStub()),
  findAll: jest.fn().mockResolvedValue([todoStub()]),
  findOne: jest.fn().mockResolvedValue(todoStub()),
  update: jest.fn().mockResolvedValue(todoStub()),
})
